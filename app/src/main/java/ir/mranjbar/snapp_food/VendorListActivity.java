package ir.mranjbar.snapp_food;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ir.mranjbar.snapp_food.adapters.VendorAdapter;
import ir.mranjbar.snapp_food.data_models.Vendor;
import ir.mranjbar.snapp_food.data_models.VendorParent;
import ir.mranjbar.snapp_food.utils.AppController;

public class VendorListActivity extends AppCompatActivity {
    private static final String TAG = "VendorListActivity";
    public static String LAT_KEY = "lat";
    public static String LON_KEY = "lon";
    double lat;
    double lon;
    private RecyclerView recyclerView;
    private VendorAdapter vendorAdapter;
    private ProgressDialog pDialog;
    private List<Vendor> vendorList = new ArrayList<>();
    private Activity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_list);
        activity = this;
        lat = getIntent().getDoubleExtra(LAT_KEY,35.7663114);
        lon = getIntent().getDoubleExtra(LON_KEY,51.4204698);

        initViews();
    }

    private void initViews() {
        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        vendorAdapter = new VendorAdapter(this, vendorList);
        recyclerView.setAdapter(vendorAdapter);
        getDataFromServer(lat , lon);

        findViewById(R.id.mapParent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, MapActivity.class);
                intent.putExtra(VendorListActivity.LAT_KEY, lat);
                intent.putExtra(VendorListActivity.LON_KEY, lon);

                startActivity(intent);
            }
        });
    }

    private void getDataFromServer(final double lat, final double lon) {
        String url ="https://newapi.zoodfood.com/mobile/v1/restaurant/new-near?lat="+lat+"&long="+lon;
        Log.e(TAG, "getDataFromServer: url : " +url);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getResources().getString(R.string.getting_data_from_Server));
        pDialog.show();
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                VendorParent vendorParent = new Gson().fromJson(response,VendorParent.class);
                List<Vendor> tempVendorList = Arrays.asList(vendorParent.getData().getVendors());
                AppController.vendorsList = vendorList;
                vendorList.addAll(tempVendorList);
                vendorAdapter.notifyDataSetChanged();

                pDialog.hide();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.hide();
                Toast.makeText(activity,"network error, rerying ...",Toast.LENGTH_SHORT).show();
                getDataFromServer(lat, lon);
            }
        });
        AppController.getInstance().addToRequestQueue(strReq, "");
    }
}
