package ir.mranjbar.snapp_food;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import ir.mranjbar.snapp_food.adapters.VendorAdapter;
import ir.mranjbar.snapp_food.utils.GPSTracker;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private static final int REQUEST_LOCATION_PERMISSION = 1;
    private Activity activity;
    private GPSTracker gpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;
        findViewById(R.id.vendorsNearMeImageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCurrentLocation();

            }
        });


    }

    private void getCurrentLocation() {

        LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (checkLocationPermission()) {

            gpsTracker = new GPSTracker(this);
            Location location = gpsTracker.getLocation();
            if (location != null) {
                Log.e(TAG, "onLocationChanged: location" + location.getLatitude() + " " + location.getLongitude());
                Intent intent = new Intent(activity, VendorListActivity.class);
                intent.putExtra(VendorListActivity.LAT_KEY, location.getLatitude());
                intent.putExtra(VendorListActivity.LON_KEY, location.getLongitude());
                startActivity(intent);
                overridePendingTransition(0, 0);
                gpsTracker.stopUsingGPS();
                finish();
            }
        }
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
            Log.e(TAG, "onLocationChanged: location" + location.getLatitude() + " " + location.getLongitude());
            Intent intent = new Intent(activity, VendorListActivity.class);
            intent.putExtra(VendorListActivity.LAT_KEY, location.getLatitude());
            intent.putExtra(VendorListActivity.LON_KEY, location.getLongitude());
            finish();
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };


    private boolean checkLocationPermission() {
        String locationPermission2 = "android.permission.ACCESS_FINE_LOCATION";
//        String locationPermission = "android.permission.ACCESS_COARSE_LOCATION";

        int locationFineRes = checkCallingOrSelfPermission(locationPermission2);
//        int locationCoarseRes = checkCallingOrSelfPermission(locationPermission);
        Log.e(TAG, "checkLocationPermission:  " + (locationFineRes == PackageManager.PERMISSION_GRANTED) );
        if (locationFineRes == PackageManager.PERMISSION_GRANTED ) {
            return true;

        } else {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(this,
                    new String[]{ Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
            return false;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.e(TAG, "onRequestPermissionsResult: permissions"+permissions[0] );
        Log.e(TAG, "onRequestPermissionsResult: grantResult"+grantResults[0] );
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentLocation();

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(MainActivity.this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
