package ir.mranjbar.snapp_food.adapters;

/**
 * Created by renjer on 2/14/18.
 */

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.text.DecimalFormat;
import java.util.List;

import ir.mranjbar.snapp_food.R;
import ir.mranjbar.snapp_food.data_models.Vendor;


/**
 * Created by renjer on 1/31/18.
 */


public class VendorAdapter extends RecyclerView.Adapter<VendorAdapter.ViewHolder> {
    private List<Vendor> values;
    private Activity activity;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView titleTextView;
        public TextView addressTextView;
        public TextView typeTextView;
        private ImageView logoImageView;
        private TextView ratingTextView;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            titleTextView = v.findViewById(R.id.titleTextView);
            addressTextView = v.findViewById(R.id.addressTextView);
            typeTextView = v.findViewById(R.id.typeTextView);
            logoImageView = v.findViewById(R.id.logoImageView);
            ratingTextView = v.findViewById(R.id.ratingTextView);
        }
    }

    public void add(int position, Vendor item) {
        values.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        values.remove(position);
//        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public VendorAdapter(Activity activity, List<Vendor> myDataset) {
        values = myDataset;
        this.activity = activity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public VendorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                    int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.vendor_list_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        VendorAdapter.ViewHolder vh = new VendorAdapter.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final VendorAdapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.titleTextView.setText(values.get(position).getTitle());
        holder.addressTextView.setText(values.get(position).getAddress());
        holder.ratingTextView.setText(String.valueOf(
                new DecimalFormat("##.#").format(values.get(position).getRating())));
        holder.typeTextView.setText(values.get(position).getDescription());
        holder.addressTextView.setText(values.get(position).getAddress());
//        DecimalFormat df = new DecimalFormat();
//        df.setMaximumFractionDigits(2);
        Glide
                .with(activity)
                .load(values.get(position).getLogo())
                .into(holder.logoImageView);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return values.size();
    }

}
