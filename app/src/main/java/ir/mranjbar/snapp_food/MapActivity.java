package ir.mranjbar.snapp_food;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import ir.mranjbar.snapp_food.data_models.Vendor;
import ir.mranjbar.snapp_food.utils.AppController;

public class MapActivity extends AppCompatActivity {
    private static final String TAG = "MapActivity";
    MapView mMapView;
    private GoogleMap googleMap;
    private GoogleMap map;
    double lat;
    double lon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        lat = getIntent().getDoubleExtra(VendorListActivity.LAT_KEY,35.7663114);
        lon = getIntent().getDoubleExtra(VendorListActivity.LON_KEY,51.4204698);
        SupportMapFragment mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        map = mapFragment.getMap();

        if (map != null) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat , lon), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(lat,lon))      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(90)                // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
        for (Vendor vendor : AppController.vendorsList) {
            int iconId;
            if (vendor.isOpen()) {
                iconId = R.drawable.restaurant_open;
            } else {
                iconId = R.drawable.restaurant_close;

            }
            LatLng tempLoc = new LatLng(vendor.getLat(), vendor.getLon());
            map.addMarker(new MarkerOptions().position(tempLoc).title(vendor.getTitle()).
                    snippet(vendor.getDescription()).icon(BitmapDescriptorFactory
                    .fromResource(iconId)));

        }
    }


}
