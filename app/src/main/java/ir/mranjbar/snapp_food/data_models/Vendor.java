package ir.mranjbar.snapp_food.data_models;

/**
 * Created by renjer on 2/14/18.
 */

public class Vendor {
    private long id;
    private String title;
    private String distance;
    private float lat;
    private float lon;
    private String address;
    private float rating;
    private String logo;
    private String vendorCode;
    private String description;
    private boolean isOpen;

    public Vendor(long id, String title, String distance, float lat, float lon,
                  String address, float rating, String logo, String vendorCode, String description, boolean isOpen) {
        this.id = id;
        this.title = title;
        this.distance = distance;
        this.lat = lat;
        this.lon = lon;
        this.address = address;
        this.rating = rating;
        this.logo = logo;
        this.vendorCode = vendorCode;
        this.description = description;
        this.isOpen = isOpen;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }
}
