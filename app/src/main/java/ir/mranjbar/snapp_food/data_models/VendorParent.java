package ir.mranjbar.snapp_food.data_models;

/**
 * Created by renjer on 2/14/18.
 */

public class VendorParent {

    private Data data;

    public VendorParent(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data{
        Vendor []  vendors;
        public Vendor[] getVendors() {
            return vendors;
        }

        public void setVendors(Vendor[] vendors) {
            this.vendors = vendors;
        }
    }

}
